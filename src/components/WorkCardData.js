
import pro1 from '../assets/note_img.jpg'
import pro2 from '../assets/weather_img.jpg'
import pro3 from '../assets/pro2.jpeg'


const ProjectCardData=[
    {
        imgsrc: pro1,
        title:"Javascript Note App",
        text:"This is the app based on html,css,and,javascript,helps to create notes, and we can save and delete the notes also",
        view:"https://chandninegi.github.io/Notes_App/"
    },
    {
        imgsrc: pro2,
        title:"API Based Weather App ",
        text:"This is the weather app maked with the help of html,css,javacsript and react.This app tells you the current weather of the entered city",
        view:"https://github.com/dashboard"
    },
    {
        imgsrc: pro3,
        title:"Registration Form Website",
        text:"This is the registration form maked with the help of html,css,js,nodejs,mongodb and express.",
        view:"https://github.com/dashboard"
    }
];
export default ProjectCardData;